package com.example.myapplication

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class Counter : Service() {
    private val binder = MyServiceBinder()
    private var value = 0

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    inner class MyServiceBinder : Binder() {
        fun getService() = this@Counter
    }

    fun increment() {
        value++
    }

    fun decrement() {
        value--
    }

    fun showValue() {
        Toast.makeText(this, "$value", Toast.LENGTH_SHORT).show()
    }
}