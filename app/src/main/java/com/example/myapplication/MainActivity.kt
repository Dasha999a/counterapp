package com.example.myapplication

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private lateinit var s : Counter
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as Counter.MyServiceBinder
            s = binder.getService()
        }

        override fun onServiceDisconnected(name: ComponentName?) {

        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Intent(this, Counter::class.java).also {
            bindService(it, connection, Context.BIND_AUTO_CREATE)
        }

        val incrementButton : Button = findViewById(R.id.increment)
        val decrementButton : Button = findViewById(R.id.decrement)
        val showValueButton : Button = findViewById(R.id.present_value)

        incrementButton.setOnClickListener{s.increment()}
        decrementButton.setOnClickListener{s.decrement()}
        showValueButton.setOnClickListener{s.showValue()}
    }
}